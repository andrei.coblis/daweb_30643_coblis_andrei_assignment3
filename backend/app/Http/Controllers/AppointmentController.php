<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Appointment;

class AppointmentController extends Controller
{
    //
    function addAppointment(Request $req)
    {
        $appointment = new Appointment;
        $appointment->doctor = $req->input('doctor');
        $appointment->userId = $req->input('userId');
        $appointment->date = $req->input('date');
        $appointment->save();

        return $appointment;
    }

    function getAppointments()
    {
        return Appointment::all();
    }
}
