<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    //
    function register(Request $req)
    {
        $user = new User;
        $user->name=$req->input('name');
        $user->email=$req->input('email');
        $user->password=$req->input('password');
        $user->medicalServiceId=$req->input('medicalServiceId');
        $user->save();
        return $user;
    }

    function login(Request $req)
    {
        $user = User::where('email', $req->email)->first();
        if(!$user || ($req->password != $user->password))
        {
            return ["error" => "Email or password doesn't match"];
        } 
        return $user;
    }

    function updateUser($id, Request $req)
    {
        $user = User::find($id);
        $user->name=$req->input('name');
        $user->email=$req->input('email');
        if($req->input('password'))
        {
            $user->password=$req->input('password');
        }
        $user->medicalServiceId=$req->input('medicalServiceId');
        $user->save();
        return $user;
    }
}
