import axios from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, FormControl, FormLabel, Table } from "react-bootstrap";
import Datepicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

export default function Appointment() {
  const [date, setDate] = useState(new Date());
  const [doctor, setDoctor] = useState();
  const [appointments, setAppointments] = useState();

  async function fetchData() {
    await axios.get("http://localhost:8000/api/appointments").then((res) => {
      setAppointments(res.data);
      console.log(appointments);
    });
  }

  useEffect(() => {
    fetchData();
  }, []);

  async function handleSubmit(e) {
    e.preventDefault();
    const userId = JSON.parse(localStorage.getItem("user-info")).id;
    const dateFormatted = moment(date).format("YYYY-MM-DD");
    await axios.post("http://localhost:8000/api/addappointment", {
      userId: userId,
      doctor: doctor,
      date: dateFormatted
    });
  }

  return (
    <div>
      <h1>Appointment</h1>
      <Form
        style={{
          paddingLeft: "100px",
          paddingRight: "100px",
          paddingBottom: "30px"
        }}
      >
        <FormLabel>Select doctor</FormLabel>
        <FormControl as="select" onChange={(e) => setDoctor(e.target.value)}>
          <option>Doctor 1</option>
          <option>Doctor 2</option>
        </FormControl>
      </Form>
      <Datepicker selected={date} onChange={(date) => setDate(date)} />
      <br />
      <Button onClick={handleSubmit} style={{ margin: "30px" }}>
        Submit Appointment
      </Button>

      <Table striped bordered hover>
        <thead>
          <th>Doctor</th>
          <th>Date</th>
        </thead>
        <tbody>
          {appointments &&
            appointments.map((item) => {
              return (
                <tr>
                  <td>{item.doctor}</td>
                  <td>{item.date}</td>
                </tr>
              );
            })}
        </tbody>
      </Table>
    </div>
  );
}
