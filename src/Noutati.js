import React, { Children } from "react";
import xmlText from "../xml/test.xml";
import { withTranslation } from "react-i18next";

class Noutati extends React.Component {
  state = {
    generalInformation: "",
    rightToAccess: "",
    managementOfPersonalData: ""
  };

  componentDidMount() {
    var XMLParser = require("react-xml-parser");
    var xml = new XMLParser().parseFromString(xmlText);
    this.setState({ generalInformation: xml.children[0].value });
    this.setState({ rightToAccess: xml.children[1].value });
    this.setState({ managementOfPersonalData: xml.children[2].value });
  }

  render() {
    const { t } = this.props;
    return (
      <div>
        <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>
          {t("News")}
        </h1>
        <div>
          <div
            style={{
              backgroundColor: "#20A0D8",
              color: "white",
              marginRight: "200px",
              marginLeft: "200px",
              fontFamily: "Comfortaa"
            }}
          >
            {t("News_title_1")}
          </div>
          <p
            style={{
              fontFamily: "RobotoCondensed",
              fontSize: "20px",
              color: "#2D2D3A",
              marginRight: "200px",
              marginLeft: "200px",
              backgroundColor: "white"
            }}
          >
            {t(this.state.generalInformation.toString())}
          </p>
        </div>
        <div
          style={{
            backgroundColor: "#20A0D8",
            color: "white",
            marginRight: "200px",
            marginLeft: "200px",
            fontFamily: "Comfortaa"
          }}
        >
          {t("News_title_2")}
        </div>
        <div>
          <p
            style={{
              fontFamily: "RobotoCondensed",
              fontSize: "20px",
              color: "#2D2D3A",
              marginRight: "200px",
              marginLeft: "200px",
              backgroundColor: "white"
            }}
          >
            {t(this.state.rightToAccess.toString())}
          </p>
        </div>
        <div
          style={{
            backgroundColor: "#20A0D8",
            color: "white",
            marginRight: "200px",
            marginLeft: "200px",
            fontFamily: "Comfortaa"
          }}
        >
          {t("News_title_3")}
        </div>
        <div>
          <p
            style={{
              fontFamily: "RobotoCondensed",
              fontSize: "20px",
              color: "#2D2D3A",
              marginRight: "200px",
              marginLeft: "200px"
            }}
          >
            {t(this.state.managementOfPersonalData.toString())}
          </p>
        </div>
      </div>
    );
  }
}
export default withTranslation()(Noutati);
