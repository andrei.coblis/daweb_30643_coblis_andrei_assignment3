import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import "./login.css";

export default function Login() {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const history = useHistory();

  async function handleSubmit(e) {
    e.preventDefault();
    let item = { email, password };
    let result = await fetch("http://localhost:8000/api/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json"
      },
      body: JSON.stringify(item)
    });

    result = await result.json();
    localStorage.setItem("user-info", JSON.stringify(result));
    window.location = "/";
  }
  return (
    <form className="box" onSubmit={handleSubmit}>
      <h1>Login</h1>
      <input
        type="text"
        name=""
        placeholder="Email"
        onChange={(e) => setEmail(e.target.value)}
      ></input>
      <input
        id="passwordInput"
        type="password"
        name=""
        placeholder="Password"
        onChange={(e) => setPassword(e.target.value)}
      ></input>
      <input type="submit" name="" value="Login"></input>
    </form>
  );
}
