import "./styles.css";
import { Route, Switch, Link, BrowserRouter as Router } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./Home";
import Contact from "./Contact";
import DespreNoi from "./DespreNoi";
import Medici from "./Medici";
import Noutati from "./Noutati";
import ServiciiTarife from "./ServiciiTarife";
import LanguageSelect from "./languageSelect";
import React, { Suspense, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import Login from "./Login";
import Register from "./Register";
import Profile from "./Profile";
import Logout from "./Logout";
import Appointment from "./Appointment";

export default function App() {
  const { t } = useTranslation();
  const [userInfo, setUserInfo] = useState("");

  useEffect(() => {
    setUserInfo(localStorage.getItem("user-info"));
    console.log(userInfo);
  }, []);

  return (
    <div className="App">
      <Suspense fallback="loading">
        <header>
          <Router>
            <div className="nav-bar">
              {userInfo && (
                <ul>
                  <li className="nav-bar-item" id="nav-bar-logo">
                    <img src={require("/img/dentaWeb.png")} alt="logo"></img>
                  </li>
                  <li className="nav-bar-item" id="nav-bar-home">
                    <Link to="/">{t("Home")}</Link>
                  </li>
                  <li className="nav-bar-item">
                    <LanguageSelect />
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Noutati">{t("News")}</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Contact">Contact</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Medici">{t("Doctors")}</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/DespreNoi">{t("About Us")}</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/ServiciiTarife">{t("Services")}</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Profile">Profile</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Logout">Logout</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Appointment">Appointment</Link>
                  </li>
                </ul>
              )}
              {!userInfo && (
                <ul>
                  <li className="nav-bar-item" id="nav-bar-logo">
                    <img src={require("/img/dentaWeb.png")} alt="logo"></img>
                  </li>
                  <li className="nav-bar-item" id="nav-bar-home">
                    <Link to="/">{t("Home")}</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Login">Login</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Register">Register</Link>
                  </li>
                  <li className="nav-bar-item">
                    <LanguageSelect />
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Noutati">{t("News")}</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Contact">Contact</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/Medici">{t("Doctors")}</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/DespreNoi">{t("About Us")}</Link>
                  </li>
                  <li className="nav-bar-item">
                    <Link to="/ServiciiTarife">{t("Services")}</Link>
                  </li>
                </ul>
              )}
            </div>
            <Switch>
              <Route exact path="/">
                <Home />
              </Route>
              <Route exact path="/Noutati">
                <Noutati />
              </Route>
              <Route exact path="/Contact">
                <Contact />
              </Route>
              <Route exact path="/DespreNoi">
                <DespreNoi />
              </Route>
              <Route exact path="/Medici">
                <Medici />
              </Route>
              <Route exact path="/ServiciiTarife">
                <ServiciiTarife />
              </Route>
              <Route exact path="/Login">
                <Login />
              </Route>
              <Route exact path="/Register">
                <Register />
              </Route>
              <Route exact path="/Profile">
                <Profile />
              </Route>
              <Route exact path="/Logout">
                <Logout />
              </Route>
              <Route exact path="/Appointment">
                <Appointment />
              </Route>
            </Switch>
          </Router>
        </header>
      </Suspense>
    </div>
  );
}
