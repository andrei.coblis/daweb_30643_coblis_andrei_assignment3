import { Button } from "react-bootstrap";
import Axios from "axios";
import { useTranslation } from "react-i18next";
export default function Contact() {
  const { t } = useTranslation();
  return (
    <div>
      <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>
        {t("Contact_title")}
      </h1>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "RobotoCondensed"
        }}
      >
        <form>
          <input
            style={{ marginTop: "10px", marginBottom: "10px" }}
            type="text"
            placeholder={t("Name")}
            id="nameInput"
          ></input>
          <br />
          <input
            style={{ marginTop: "10px", marginBottom: "10px" }}
            type="text"
            placeholder="Email"
            id="emailInput"
          ></input>
          <br />
          <input
            style={{ marginTop: "10px", marginBottom: "10px" }}
            type="text"
            placeholder={t("Subject")}
            id="subjectInput"
          ></input>
          <br />
          <input
            style={{ marginTop: "10px", marginBottom: "10px" }}
            type="text"
            placeholder={t("Your_message")}
            id="contentInput"
          ></input>
          <br />
          <Button
            onClick={() => {
              Axios.post("http://127.0.0.1:8000/", {
                name: document.getElementById("nameInput").value,
                email: document.getElementById("emailInput").value,
                subject: document.getElementById("subjectInput").value,
                content: document.getElementById("contentInput").value
              });
              fetch("http://127.0.0.1:8000/");
              document.getElementById("nameInput").value = "";
              document.getElementById("emailInput").value = "";
              document.getElementById("subjectInput").value = "";
              document.getElementById("contentInput").value = "";
            }}
            style={{ marginBottom: "10px" }}
          >
            {t("Send_email")}
          </Button>
        </form>
      </div>
    </div>
  );
}
