import { Link } from "react-router-dom";
import { useTranslation } from "react-i18next";
export default function Medici() {
  const { t } = useTranslation();
  return (
    <div>
      <h1 style={{ fontFamily: "Comfortaa", padding: "20px" }}>
        {t("Doctors")}
      </h1>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          fontFamily: "Comfortaa"
        }}
      >
        Dr. Hikaru Nakamura
      </div>
      <img
        alt="doctor"
        src={require("/img/Dr man.png")}
        style={{ height: "200px", margin: "10px" }}
      ></img>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        {t("Doctor_text_1")}
      </p>
      <Link to="/contact" className="btn btn-primary">
        Contact
      </Link>
      <div
        style={{
          backgroundColor: "#20A0D8",
          color: "white",
          marginRight: "200px",
          marginLeft: "200px",
          marginTop: "15px",
          fontFamily: "Comfortaa"
        }}
      >
        Dr. Anna Cramling
      </div>
      <img
        alt="doctor"
        src={require("/img/Dr woman.png")}
        style={{ height: "200px", margin: "10px" }}
      ></img>
      <p
        style={{
          fontFamily: "RobotoCondensed",
          fontSize: "20px",
          color: "#2D2D3A",
          marginRight: "200px",
          marginLeft: "200px",
          backgroundColor: "white"
        }}
      >
        {t("Doctor_text_2")}
      </p>
      <Link to="/contact" className="btn btn-primary">
        Contact
      </Link>
    </div>
  );
}
